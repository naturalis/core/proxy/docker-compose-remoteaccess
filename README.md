# docker-compose-reverseproxy

docker-compose template for configuring a remote access server based on Traefik for
(internal) services not running as Docker containers and several other components

The definition of services that need a reverse proxy should be provided as an
external dynamic Traefik configuration file. By default this file is located
relatively to this docker-compose definitien in
`../config/traefik-dynamic-config.toml`. You can override this location on the
host system by setting the TRAEFIK_DYNAMIC_CONFIG environment variable. 

The config file could like for example like this:

```toml
[http]
  [http.routers]
    [http.routers.service1]
      entryPoints = ["websecure"]
      rule = "Host(`service1.example.com`)"
      service = "service1"
      [http.routers.service1.tls]
        certResolver = "route53"

    [http.routers.service2]
      entryPoints = ["websecure"]
      rule = "Host(`service2.example.com`)"
      service = "service2"
      [http.routers.service2.tls]
        certResolver = "route53"

  [http.services]
    [http.services.service1]
      [http.services.service1.loadBalancer]
        [[http.services.service1.loadBalancer.servers]]
          url = "http://172.16.1.10:8080"

    [http.services.service2]
      [http.services.service2.loadBalancer]
        [[http.services.service2.loadBalancer.servers]]
          url = "http://172.16.1.11:8081"
```

Route53 is hard coded as certificate resolver (at the moment).

In order to run this compose file you need to set the following environment
variables, for example by using a `.env` file:

* `TRAEFIK_VERSION`: you need to specify the specific tag of the Traefik
  container. For example: `v2.10`. Traefik is now centrally managed
* `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`: credentials with access to
